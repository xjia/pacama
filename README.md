Page Caching Manager
=====================

Motivating Example
-------------------
Suppose you are building a website for publishing pages of texts.
In case the number of visitors exceeds the capacity of your app 
server, it is essential to cache the pages in order to respond 
quickly. In this simple example, a page has attributes such as 
path, title, content, and theme, in which the theme determines the 
style, e.g. normal, bold, or italic, for the sake of simplicity.
Page path is only used for specifying where the generated static 
HTML file should be stored and served.

Pacama serves the caching configurations and programs in a way 
like a J2EE application server. Pacama can be started in embedded 
mode and can only do in-process communication. If you want to use
pacama in a distributed environment, you may write your own wrapper
for it by using TCP or other networking techniques.

```xml
<!-- a model may be virtual but needs a concrete class for it -->
<model name="page" class="pacama.example.motiv.Page">
  <property name="path"/> <!-- type defaults to java.lang.String -->
  <property name="title"/>
  <property name="content"/>
  <property name="theme" type="pacama.example.motiv.Theme"/>
</model>
<view name="content" class="pacama.example.motiv.ContentView" priority="10">
  <concern-property name="page content" model="page" property="content"/>
  <concern-property name="page theme" model="page" property="theme"/>
  <!-- pacama will subclass ContentView dynamically -->
  <!-- getOutcome() will be cached in memory -->
</view>
<view name="page" class="pacama.example.motiv.PageView" priority="5">
  <concern-model name="page" model="page"/>
  <concern-view name="content view" view="content"/>
</view>
<view name="page saver" class="pacama.util.FileSaver" priority="1">
  <!-- This is for page persistence -->
  <concern-view name="view" view="page"/>
  <property name="path">/var/www/pages</property>
  <property name="suffix">.html</property>
  <property name="max size" type="java.lang.Integer">65536</property>
</view>
```

```java
final Pacama pcm = new Pacama(Main.class, "pacama-config.xml");

final Page page = new Page();
page.setPath("some_slot_name");
page.setTitle("Test Page");
page.setContent("This is a page for test.");
page.setTheme(new BoldTheme());

// Test 1 -- this will cause 3 updates
page.setTitle("Test Page with Another Title");
page.setContent("And content changed, too.");
pcm.updateModel(page);
pcm.updateProperty(page, "title");
pcm.updateProperty(page, "content");

// Test 2 -- this will cause only 1 update of the model
final Transaction tx = pcm.beginTransaction();
page.setTitle("Test Page within Transaction");
page.setContent("Will be different in a transaction.");
tx.updateModel(page);
tx.updateProperty(page, "title");
tx.updateProperty(page, "content");
tx.commit();	// this transaction is closed on commit

pcm.dispose();
```

`priority` of a view is used to determine a full order, instead of a partial order,
in the process of topology sorting. Endpoints like models trigger the cache refreshing
process. When there is a tie or a loop, it is resolved by comparing the priorities.
If the priorities make a tie, report a fatal error. So it is the programmer's or the 
administrator's responsibility to guarantee there exists a full-order topology sorting.

Pacama is an injective tool which makes use of reflection mechanism extensively.
Pacama also provides utility classes for handling database connections and files.
For example, `PageRenderer` and `PageGenerator` may find it easy to extend the utilities.
Pacama is easy to implement in Java, but can also be available in other languages
like Ruby, PHP, etc.
