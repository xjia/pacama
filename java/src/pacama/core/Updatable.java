package pacama.core;

public interface Updatable {

	public void updateModel(Object model);
	public void updateProperty(Object model, String property);
}
