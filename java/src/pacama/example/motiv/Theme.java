package pacama.example.motiv;

public interface Theme {

	public String render(String raw);
}
