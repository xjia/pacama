package pacama.example.motiv;

import pacama.core.View;

public class ContentView implements View {

	private String content;
	private Theme theme;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	
	@Override
	public String getOutcome() {
		return getTheme().render(getContent());
	}
}
