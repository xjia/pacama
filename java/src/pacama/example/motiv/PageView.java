package pacama.example.motiv;

import pacama.util.SavableView;

public class PageView implements SavableView {

	private Page page;
	private ContentView contentView;
	
	public Page getPage() {
		return page;
	}
	
	public void setPage(Page page) {
		this.page = page;
	}

	public ContentView getContentView() {
		return contentView;
	}

	public void setRenderer(ContentView contentView) {
		this.contentView = contentView;
	}

	@Override
	public String getOutcome() {
		return String.format(
				"<html><head><title>%s</title></head><body>%s</body></html>",
				getPage().getTitle(), getContentView().getOutcome());
	}

	@Override
	public String getPersistentId() {
		return getPage().getPath();
	}
}
