package pacama.example.motiv;

import pacama.core.Pacama;
import pacama.core.Transaction;

public class Main {

	public static void main(String[] args) {
		final Pacama pcm = new Pacama(Main.class, "pacama-config.xml");
		
		final Page page = new Page();
		page.setPath("some_slot_name");
		page.setTitle("Test Page");
		page.setContent("This is a page for test.");
		page.setTheme(new BoldTheme());
		
		// Test 1 -- this will cause 3 updates
		page.setTitle("Test Page with Another Title");
		page.setContent("And content changed, too.");
		pcm.updateModel(page);
		pcm.updateProperty(page, "title");
		pcm.updateProperty(page, "content");
		
		// Test 2 -- this will cause only 1 update of the model
		final Transaction tx = pcm.beginTransaction();
		page.setTitle("Test Page within Transaction");
		page.setContent("Will be different in a transaction.");
		tx.updateModel(page);
		tx.updateProperty(page, "title");
		tx.updateProperty(page, "content");
		tx.commit();	// this transaction is closed on commit
		
		pcm.dispose();
	}
}
