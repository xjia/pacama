package pacama.example.motiv;

public class BoldTheme implements Theme {

	@Override
	public String render(String raw) {
		return String.format("<b>%s</b>", raw);
	}

}
