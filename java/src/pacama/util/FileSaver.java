package pacama.util;

public class FileSaver {

	private SavableView view;
	private String path;
	private String suffix;
	private Integer maxSize;
	
	public SavableView getView() {
		return view;
	}
	
	public void setView(SavableView view) {
		this.view = view;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public Integer getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(Integer maxSize) {
		this.maxSize = maxSize;
	}
	
	public String getOutcome() {
		// TODO persistent view
		return getView().getOutcome();
	}
}
