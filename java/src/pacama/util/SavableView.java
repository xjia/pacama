package pacama.util;

import pacama.core.View;

public interface SavableView extends View, Savable {}
