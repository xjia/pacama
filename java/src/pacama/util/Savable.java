package pacama.util;

public interface Savable {

	public String getPersistentId();
}
